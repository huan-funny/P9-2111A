let arr = [1,2,3,4,5,6];

let sum = arr.reduce((prev, current, index) => {
  /**
   * prev： 0, current: 1
   * prev: 0 + 1, current: 2
   * prev: 0 + 1 + 2, current: 3
   * ....
   */
  return prev + current;
}, 0);
console.log(sum, 'sum');

let cart = [{id: 1, price: 1, num: 1, flag: true}, {id: 2, price: 2, num: 2, flag: false}, {id: 3, price: 3, num: 3, flag: true}];

let toalSum = cart.reduce((prev, current) => {
  /**
   * prev: 0, 走了判断条件去相加
   * prev: 1*1, 
   * prev: 1*1, 去相加
   */
  if (current.flag) {
    return prev + current.price * current.num
  } else {
    return prev;
  }
}, 0);
console.log(toalSum, 'toalSum');

// reverse: 反转数组，返回新的数组，原数组也会发生改变
let newArr = arr.reverse();
console.log(newArr, 'newArr', arr, 'arr')

// flat: 扁平化数组，里面的参数是你要扁平化的层级, Infinity无限扁平化，不管多少层都转化一维数组
let moreArr = [1,2,3, [4,[5,[6]]], [7]]
// [1,2,3,4,5,6,7]
let newMoreArr = moreArr.flat(3);
console.log(newMoreArr, 'newMoreArr')

let moreArr1 = [ [1, 2, 2], [3, 4, 5, 5], [6, 7, 8, 9, [11, 12, [12, 13, [14] ] ] ], 10];
let newMoreArr1 = moreArr1.flat(Infinity);
console.log(newMoreArr1, 'newMoreArr1');

// Array.from() 是伪数组转成数组
// 伪数组：有数组的长度，没有数组的方法
let str = '12345678qwer';
let num = 123456
let strArr = Array.from(str);
console.log(strArr, 'strArr');

let setArr = new Set([1,2,3,3,2,1]);
console.log(setArr, 'setArr');

/**
 * 扩展：将 数组扁平化 并 去除其中重复数据，最终得到一个升序且不重复的数组 eg: [ [1, 2, 2], [3, 4, 5, 5], [6, 7, 8, 9, [11, 12, [12, 13, [14] ] ] ], 10];
 */
let arrK = [ [1, 2, 2], [3, 4, 5, 5], [6, 7, 8, 9, [11, 12, [12, 13, [14] ] ] ], 10];
let newArrK = arrK.flat(Infinity);
let newArrK1 = Array.from(new Set(newArrK));
console.log(newArrK1.sort((a, b) => a - b), 'newArrK1.sort()')

// 判断数组的方法 instanceof constructor Object.prototype.toString.call() Array.isArray()

// 判断是否为数组，返回值布尔值
console.log(Array.isArray(''), 'Array.isArray([])');

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cart: []
  },
  getters: {
  },
  mutations: {
    addCart(state, obj) {
      let targetItem = state.cart.find(item => item._id === obj._id);
      if (targetItem) {
        targetItem.num += 1;
      } else {
        state.cart.push({
          ...obj,
          flag: false,
          num: 1
        });
      }
    },
    changeCart(state, flag) {
      state.cart.forEach(item => item.flag = flag);
    },
    delCart(state) {
      state.cart = state.cart.filter(item => !item.flag)
    }
  },
  actions: {
  },
  modules: {
  }
})

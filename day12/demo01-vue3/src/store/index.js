import { createStore } from 'vuex'

export default createStore({
  state: {
    total: 100
  },
  getters: {
  },
  mutations: {
    setTotal(state) {
      state.total += 10;
    }
  },
  actions: {
  },
  modules: {
  }
})

let str1 = '合计结算'
let str2 = 5;

let str3 = str1 + str2;
// 合计结算5
let str4 = `${str1}${str2}`;
// concat 拼接，返回一个新的字符串
let str5 = str1.concat(str2, str4)
console.log(str5, 'str5')

// includes: 一个字符串是否包含另一个字符串，返回值是一个布尔值
let keyword = ''; // 输入框搜索的内容
let list = [{id: 1, name: '张三'}, {id: 2, name: '张思'}, {id: 3, name: '李四'}]
let newList = list.filter(item => item.name.includes(keyword));

// indexOf: 查找符合元素的下标，返回第一个满足条件的下标, 若没找到目标元素，会返回-1
let value = 'dog'
let longstr = 'daomdogdoGdagdog';
let index = longstr.indexOf(value)
console.log(index !== -1, 'index', index >= 0);

// lastIndexOf: 是从后往前找，找到满足条件的第一个元素的下标，若没找到目标元素，会返回-1
let lastIndex = longstr.lastIndexOf(value)
console.log(lastIndex, 'lastIndex');

// slice: 截取字符串，(开始位置，结束位置) 特点：包左不包右, 不改变原字符串的数据，会返回一个新的字符串
let newStr = longstr.slice(4, 8);
console.log(newStr, 'newstr', longstr, 'longstr');
// substr(开始位置, 长度)
let newStr1 = longstr.substr(4, 4);
// substring(开始位置, 结束位置)
longstr.substring(4, 8)

// replace(要替换的元素/正则表达式，替换后的元素): 替换
// 'daomdogdoGdagdog'
let reStr = longstr.replace('dog', 'cat')
let reStrAll = longstr.replaceAll('dog', 'cat');
console.log(reStr, 'reStr', reStrAll, 'reStrAll');
// 用正则/g可以匹配字符串所有满足条件的字符串
// /i 可以让正则 忽略 大小写
// let reStr1 = longstr.replace(/dog/gi, 'cat')
// console.log(reStr1, 'reStr1');

let htmlStr = '<p><img src="log.png">666666yanglegeyang<img src="" />详情对方过后就看我儿法国红酒</p>'

// \ 转义符
let newhtmlStr = htmlStr.replace(/\<img/g, '<img style="width: 100%"')
console.log(newhtmlStr, 'newhtmlStr');

let zhStr = '中国China 汉语Chinese'
let zhStr1 = zhStr.toLowerCase(); // 将字符串中 大写字符 换成 小写字符
console.log(zhStr1, 'zhStr1')

zhStr.toUpperCase(); // 将字符串中 小写字符 换成 大写字符
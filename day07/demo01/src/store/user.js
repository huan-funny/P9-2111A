// 模块中同样包含vuex五大核心
const user = {
  namespaced: true, // 开启模块的命名空间
  state: {
    student: [{id: 7, name: '张三', age: 18}, {id: 8, name: '李四', age: 18}]
  },
  getters: {
    firstStudent(state) {
      return state.student[0]
    },
    totalMoney(state) {
      return 11111
    }
  },
  mutations: {
    addStudent(state, obj) {
      state.student.push(obj)
    }
  },
  actions: {
    queryStudent({ dispatch, commit, state }) {
      console.log('objectobjectobjectobject');
    }
  },
  modules: {}
}
export default user;

import Vue from 'vue';
import moment from 'moment';

// 年-月-日
Vue.filter('dateFormat', (val, str = '-') => {
  console.log(val, 'val');
  // let date = new Date(val);
  // const year = date.getFullYear(); // 年
  // const month = date.getMonth() + 1; // 月
  // const day = date.getDate() // 日

  // return `${year}${str}${month}${str}${day}`
  return moment(val).format(`YYYY${str}MM${str}DD`)
})

Vue.filter('timeFormat', (val, str = '-') => {
  console.log(val, 'val');
  // let date = new Date(val);
  // const year = date.getFullYear(); // 年
  // const month = date.getMonth() + 1; // 月
  // const day = date.getDate() // 日

  // const hour = date.getHours()// 时
  // const min = date.getMinutes()
  // const second = date.getSeconds()

  // return `${year}${str}${month}${str}${day} ${hour}:${min}:${second}`
  return moment(val).format(`YYYY${str}MM${str}DD HH:mm:ss`)
})

let arr = [1,2,3,4,5,6];

// shift: 头部删除；返回值是删除的元素；会改变原数组
let prop = arr.shift();
// console.log(prop, 'prop'); // 1
// console.log(arr, 'arr');

// pop: 末尾删除;返回值是删除的元素；会改变原数组
let prop1 = arr.pop();
console.log(prop1, 'prop1');
// splice
注意点：
  1、仓库名称 与 本地文件夹名称 要一致！！！
### 克隆/复制 代码
  git clone <克隆地址>

### 拉取代码 / 更新代码
  主分支：master / main
  git pull origin <分支名称>

### 全局配置 用户名 和 邮箱
git config --global user.name <用户名>
git config --global user.email <邮箱>

### 查看本地配置信息
git config --list

:q 不保存退出
:wq 保存退出

### 项目git初始化
  git init

### 设置仓库地址
git remote add origin <仓库地址>

### 删除仓库地址
git remote remove origin <仓库地址>

### 添加变更内容到暂存区
git add .

### 提交代码到本地
git commit -m '提交内容的说明'

### 提交代码到远程
git push origin <分支名>


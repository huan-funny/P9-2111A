let person = {
  name: '张山',
  age: 18,
  job: 1500,
  work: '',
  work1: undefined
};
// 判断一个对象是否含有某个属性
let hasName = person.hasOwnProperty('name')
console.log(hasName, 'hasName');
// let hasAge = Object.hasOwn(person, 'age');
// console.log(hasAge, 'hasAge');

// 获取对象中所有的属性值, 返回一个属性值列表
let valList = Object.values(person);
console.log(valList, 'valList')
// 获取对象中所有的属性名列表
let keyList = Object.keys(person);
console.log(keyList, 'keyList')

// 判断一个对象是否为空对象？（一个对象里是否有属性？）
// Object.values() 、 Object.keys()


// let list = [];
// [{
//   id: 1,
//   list: [1224,3444],
// }, {
//   id: 2,
//   list: [233,5556]
// }]
// let lmap = {
//   1: [1224,3444],
//   2: [233,5556]
// }

// Object.create() 将一个对象赋值给这个新对象的原型
let person2 = Object.create(person)
// Object.create(null); 创建一个没有原型的对象，没有原型的方法

// 冻结对象, 冻结的对象无法修改，在vue中使用可减少对数据的监听，提高效率
let freezeObj = Object.freeze(person)
freezeObj.age = 19

// Object.is() 内部是通过===判断，判断两个NaN是例外
console.log(Object.is(false, 0)) // false
console.log(Object.is(1, '1')) // false
console.log(Object.is(0, 0)) // true
console.log(Object.is(-0, 0)) // false
console.log(Object.is(undefined, null)) // false
/**
 * NaN === NaN
    false
 */
console.log(Object.is(NaN, NaN)); // true

// isPrototypeOf 一个对象的原型与另外一个对象的原型是否相等
function Fun() {}
Fun.prototype.sayHello = function () {}

let obj = new Fun();

let newObj = Object.create(obj);

Fun.prototype.isPrototypeOf(obj); // true



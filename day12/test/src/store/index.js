import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    bgColor: 'red',
    cart: []
  },
  getters: {
  },
  mutations: {
    setBgColor(state) {
      state.bgColor = '#' + Math.random().toString(16).slice(2, 8);
    },
    addCart(state, obj) {
      state.cart.push({
        ...obj,
        status: false
      });
    },
    // 改变正在看
    changeStatus(state, id) {
      let index = state.cart.findIndex(item => item.id == id);
      state.cart[index].status = true;
    },
    // 改变已看完
    changeStatused(state, id) {
      let index = state.cart.findIndex(item => item.id == id);
      state.cart[index].status = false;
    },
    // 改变通用
    changeStatusCart(state, id) {
      let index = state.cart.findIndex(item => item.id == id);
      state.cart[index].status = !state.cart[index].status;
    }
  },
  actions: {
  },
  modules: {
  }
})

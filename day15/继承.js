// 构造函数继承,是通过call、apply、bind来改变this指向来继承的
// 缺点：没有办法继承到父类原型上的属性或方法。
/**
 * 
function Parent() {
  this.name = '李四';
  this.sayHello = function () {
    console.log('nihao')
  }
}
function Child() {
  this.age = 18;
  Parent.call(this);
  // this.name = '李四';
  // this.sayHello = function () {
  //   console.log('nihao')
  // }
}
let obj = new Child();
// obj
 */

// 原型继承
// 可以继承到父类原型上的属性或方法，会挂载子类的原型的原型上
// 缺点：改变任意一个实例对象原型上的属性或方法，其他也会跟随发生改变
function Parent() {
  this.name = '李四';
  this.sayHello = function () {
    console.log('nihao')
  }
}
function Child() {
  this.age = 18;
}
Child.prototype = new Parent();
let obj1 = new Child();
let obj2 = new Child();
obj1.__proto__.name = '王武';

// 组合继承: 可继承到对象本身和原型上，但会造成自身和原型上有一部分属性或方法重复，造成冗余
function Parent() {
  this.name = '李四';
  this.sayHello = function () {
    console.log('nihao')
  }
}
Parent.prototype.sex = '男';
function Child() {
  this.age = 18;
  Parent.call(this)
}
Child.prototype = new Parent();
let obj3 = new Child();

// class 类继承 通过extends继承父类，通过super去继承父类里的属性或方法
class Parent {
  constructor(name) {
    this.name = name;
    this.sayHello = function () {
      console.log('你好')
    }
  }
}
// new Parent() 通过实例化来调用类
class Child extends Parent {
  constructor(name) {
    super(name); // 使用位置要在子类使用this的前面使用
    this.age = 19;
    this.name = '李四';
  }
}
let obj4 = new Child('张三');

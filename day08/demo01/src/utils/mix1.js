const mix1 = {
  data () {
    return {
      num: 1
    }
  },
  created() {
    console.log(this.num, 'object');
  },
  methods: {
    addNum() {
      this.num += 1;
    }
  },
}

export default mix1;

/**
 * 闭包
 */
let b = 'eede';
function fun1() {
  let a = '接的';
  const fun2 = function() {
    console.log(a, 'a')
  }
  
  return fun2
}
let aa = fun1()
aa()
aa = null;

/**
 * 防抖：n秒内重复触发事件，重新计算时间
 */

function debounce(fn) {
  let timer = null;
  clearTimeout(timer);
  return function() {
    timer = setTimeout(() => {
      console.log(this, 'this')
      fn.apply(this, arguments)
    }, 2000)
  }
}

const fun3 = debounce((name) => {
  console.log('qwertyqwerty',name)
});

/**
 * 节流：n秒内重复触发事件，无视当前触发的，只执行最早触发一次
 */
// data() {
//   return {
//     isCanClick: true
//   }
// },
// methods: {
//   SubmitEvent() {
//     if (!this.isCanClick) return;
//     this.isCanClick = false;
//     axios.post(url).then(res => {
//       this.isCanClick = true;
//     })
//   }
// }
function throttle(fn) {
  let isCanClick = true;
  return function() {
    if (isCanClick) {
      isCanClick = false;
      setTimeout(() => {
        fn.apply(this, arguments);
        isCanClick = true;
      }, 2000);
    }
  }
}
const fun4 = throttle((name) => {
  console.log('qwertyqwerty',name)
});


import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
// 第一种：创建中央事件总线
Vue.prototype.$bus = new Vue();

new Vue({
  router,
  store,
  render: h => h(App),
  // provide () {
  //   return {
  //     axios: axios
  //   }
  // }
}).$mount('#app')

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.directive('copy', function(el, binding, vnode) {
    console.log(el, 'el');
    el.onclick = function(event) {
      console.log(event);
      let dom = document.createElement('input');
      dom.value = binding.value;
      document.body.appendChild(dom);

      dom.select(); // 选中input输入框内容
      document.execCommand('copy');
      console.log('复制成功');
      document.body.removeChild(dom);
    }
})
Vue.directive('drag', {
  bind(el, binding, vnode) {
    el.onmousedown = function(event) {
      console.log(event);
      // event.clientX 鼠标点击位置距离文档左边的距离
      // el.offsetLeft 元素盒子距离左边的距离
      const difX = event.clientX - el.offsetLeft; // 得到点击位置离盒子内边框左边的距离
      const difY = event.clientY - el.offsetTop; // // 得到点击位置离盒子内边框顶部的距离
      document.onmousemove = function(ev) {
        console.log(111111);
        let left = ev.clientX - difX < 0 ?  0 : ev.clientX - difX;
        const top = ev.clientY - difY;
        el.style.position = 'absolute';

        el.style.left = left + 'px';
        el.style.top = top + 'px';
      }
      document.onmouseup = function() {
        document.onmousedown = null;
        document.onmousemove = null;
      }
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

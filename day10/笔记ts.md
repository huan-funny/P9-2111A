### 安装TS
npm install -g typescript

### 查看版本号
tsc -v

### 创建一个ts 文件
文件名后缀为.ts

### 运行ts
tsc 文件名.ts

### 自动编译
1、tsc --init // 此时会生成一个tsconfig.json
2、vscode 菜单栏 =》终端 =》typescript =》选择tsc:监听
  或使用命令工具
  tsc -p tsconfig.json --watch


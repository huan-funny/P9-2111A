/**
 * 递归：函数自身调用自身，一定要有终止条件
 */
/**
1 2 3 4 5 。。。。。 100
sum1 = 0 + 1
sum2 = sum1 + 2
sum3 = sum2 + 3
....
sum99 = sum98 + 99
sum100 = sum99 + 100
 */

function sum(n) {
  if (n === 1) return 1;
  return sum(n - 1) + n;
}
sum(100) = sum(99) + 100
          sum(98) + 99
          sum(97) + 98
          sum(96) + 97
          sum(1) + 2
          1

          // ### 递归的典型案例：斐波那契数列
          // 已知：1、1、2、3、5、8、13、21.....
          // f(1) = 1
          // f(1) = 0 + 1; 
          // f(2) = f(1) + f(1);
          // f(3) = f(2) + f(1);
          
          
          // let arr = [1,3,2,5,7,12,67];
// 对象深拷贝 // + 数组拷贝
function deepCopy(obj) {
  if (typeof obj[key] !== 'object' || obj[key] === null) return; // 基本数据类型返回
  let newObj = {};
  for(let key in obj) {
    if (typeof obj[key] === 'object' && obj[key] !== null) {
      newObj[key] = deepCopy(obj[key])
    } else {
      newObj[key] = obj[key]
    }
  }
  return newObj;
}
let obj = {
  name: 'dde',
  a: {
    c: {
      d: 1
    }
  },
  e: null
}
deepCopy(obj)
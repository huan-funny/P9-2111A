import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// import ElementPlus from 'element-plus'

/**
 * vue2 与 vue3 的区别
 *  1、创建实例化
 *    vue2: new Vue()
 *    vue3: createApp()
 * 
 * vue2 中下载element-ui， vue3 中下载element-plus
 */
let app = createApp(App); // 创建一个实例，
app.use(store)
app.use(router)
// app.use(ElementPlus)
app.mount('#app')

/**
 * 特点：
 *  1、三种状态：pending、fulfilled、rejected
 *  2、状态一旦发生改变，不可逆，只会从pending到fulfilled，或pending到rejected
 * 
 * 解决了  回调地狱 的问题
 * 
 * promise 可实现链式调用
 */

// 回调地狱
// queryList(url, function() {
//   queryList1(url, function() {
//     ...
//   })
// })

// then、catch、finally 是promise原型上的方法

function fun1() {
  return new Promise((resolve, reject) => {
    let num = Math.random()*100;
    if (num <= 30) {
      resolve('你中奖了')
    } else {
      reject('再接再厉')
    }
  })
}
// 调用.then,.catch方法必须有返回值，也就是说它得有resolve 或 reject方法返回
/**
 * then 方法有两个回调函数参数，第一个是处理成功的回调函数，第二个是处理 失败 的回调函数
 * fun1().then((res) => {
    console.log(res, 'res')
    },(error) => {
        console.log(error, 'error')
    })
 */
  fun1().then((res) => {
      console.log(res, 'res')
  }).catch((error) => {
      console.log(error, 'error')
  }).finally(() => { // 不管promise的结果是成功还是失败，都会执行finally 函数，finally回调函数中没有参数
    console.log('promise结果返回了')
  })

  // resolve() 方法 会将promise 的状态从pending 改为 fulfilled
  Promise.resolve().then(res => {
    console.log(res, '11111')
  })
  // reject() 方法 会将promise 的状态从pending 改为 rejected
  Promise.reject('qwertyu').then(res => {
      console.log(res, '11111')
  }).catch((error) => {
      console.log(error, 'error')
  })
  // all 可以将多个promise实例对象组合成一个实例对象, 当所有的实例对象返回 成功 时，才会走then方法，但凡有一个返回的是 失败的，那就会走.catch方法
  let p1 = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('11111')
      }, 3000)
    })
  }
  let p2 = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('2222')
      }, 2000)
    })
  }

  Promise.all([p1, p2]).then(res => {
    console.log(res, 'res');
  })

  // race 可以将多个promise实例对象组合成一个实例对象, 此时多个promise实例对象谁的结果先返回，那新的实例对象的结果就是什么，它的状态是根据先返回来的那个实例对象的状态决定的
  Promise.race([p1(), p2()]).then(res => {
    console.log(res, 'res')
  }).catch(error=> {
      console.log(error, 'error')
  })

/**
 * 执行结果
 * console.log('1')
  let p = new Promise((resolve, reject) => {
      console.log('2');
      resolve('3');
      console.log('4')
  });
  Promise.resolve('7').then(res => {
      console.log(res)
  })
  p.then((res) => {
      console.log(res)
      console.log('5')
  });
  console.log('6')
  Promise.then(() => {
      console.log('8')
  })
 */

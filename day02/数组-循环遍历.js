let arr = [1,2,3,4,5,6];

for(let i = 0; i < arr.length; i++) {
  console.log(arr[i], 'arr[i]')
}

let idx = 0;
while(idx < arr.length) {
  arr[idx] += 2;
  idx++;
}
// console.log(arr, 'arr');
// [ 3, 4, 5, 6, 7, 8 ] arr

// item -= 2 => item = item -2
// forEach: 循环遍历，没有返回值, 当修改简单数组时，原数组不会发生改变；当修改复杂数组时，原数组 会！！！发生改变
arr.forEach(item => item -= 2);
// console.log(arr, 'arr'); [ 3, 4, 5, 6, 7, 8 ]

let arr1 = [{id: 1, num: 1}, {id: 2, num: 2}, {id: 3, num: 3}, {id: 4, num: 4}];
arr1.forEach(item => item.num += 2);
// console.log(arr1, 'arr1');
/**
 * [
  { id: 1, num: 3 },
  { id: 2, num: 4 },
  { id: 3, num: 5 },
  { id: 4, num: 6 }
]
 */

// map：有返回值，返回值是修改后的新数组；不会修改原数组
let newArr = arr.map(item => item -= 2);
// console.log(arr, 'arr');[ 3, 4, 5, 6, 7, 8 ]

// for in 去循环数组时，key值是数组的下标; 当循环对象时，key值是属性名
for(let key in arr) {
  console.log(arr[key], 'key');
}

let obj = {a: 1, b: 2};
for(let key in obj) {
  // obj[key] 动态访问对象中的属性的值
  console.log(obj[key], 'key')
}
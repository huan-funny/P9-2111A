import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: 'teacher',
    component: HomeView,
    // 注意！！！子路由不用加 ‘/’
    children: [{
      path: 'student',
      component: () => import('../views/Student'),
      meta: { // 路由元信息
        title: '学生管理'
      }
    }, {
      path: 'teacher',
      component: () => import('../views/Teacher'),
      meta: {
        title: '讲师管理'
      }
    }],
    beforeEnter: (to, from, next) => {
      // ...
      console.log('路由独享守卫 ---- beforeEnter')
      next();
    }
  },
  {
    path: '/detail/:id', // 动态路由
    name: 'detail',
    component: () => import('../views/Detail'),
    beforeEnter: (to, from, next) => {
      next();
    }
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
    beforeEnter: (to, from, next) => {
      // ...
      console.log('路由独享守卫 ---- beforeEnter')
      next();
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login')
  }
]

const router = new VueRouter({
  routes
})
/**
 * 路由守卫：
 *  1、全局路由守卫: beforeEach、afterEach、beforeResolve
 *  2、组件守卫: beforeRouteEnter、beforeRouteUpdate、beforeRouteLeave
 *  3、独享守卫: beforeEnter
 */
/**
 * to: 去哪
 * from: 从哪里
 * next: 放行
 */
/**
 * 路由守卫执行顺序：
 * 0:上一个路由的 beforeRouteLeave
 * 1、beforeEach
 * 2、beforeEnter
 * 3、beforeRouteEnter
 * 4、afterEach
 * 
 * 更新
 * beforeEach
 * beforeRouteUpdate
 * afterEach
 */
const whiteList = ['login', 'register', 'a'];
router.beforeEach((to, from, next)=> {
  console.log('全局路由守卫 ---- beforeEach');
  if (!localStorage.getItem('token') && !whiteList.includes(to.name)) {
    next('/login');
  } else {
    next()
  }
});

router.afterEach((to, from, next) => {
  // console.log(to, 'to')
  console.log('全局路由守卫 ---- afterEach');
  document.title = to.meta.title; // 设置页面标题头
})


export default router

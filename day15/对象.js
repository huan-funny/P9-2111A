/**
 * 面向对象：是一种编程思想，它把事务分解成一个个的对象，然后由对象之间分工合作。
 * 对象：是由属性和方法组件的无序键值对的集合
 */
// 创建对象的方式：
// 1、字面量
let obj = {
  name: '张三'
};
// 2、使用构造函数创建对象
function Fun1() {
  this.name = '李四';
}
Fun1.prototype.sayHello = function () {
  console.log('nihao')
}
let obj1 = new Fun1();
/**
 * new操作符的作用？干了什么了
 * 1、使用new创建一个实例对象
 * 2、把构造函数当中的this指向实例对象
 * 3、把构造函数的原型与实例对象的原型建立联系
 * 4、返回一个新的对象
 */

// 3、通过class创建对象
class fun2 {
  constructor() {
    this.name = '王武';
    this.sayHello = function () {
      console.log('nihao')
    }
  }
  getName = () => {
    console.log(this.name, 'name')
  }
}
let obj2 = new fun2()
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import user from './user';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cartList: [{id: 1, title: '红楼梦', price: 100}, {id: 2, title: '水浒传', price: 90}, { id: 3, title: '西游记', price: 80}, { id: 4, title: '三国演义', price: 70}]
  },
  getters: {
    totalMoney(state) {
      let sum = 0;
      state.cartList.forEach(item => sum += Number(item.price))
      return sum;
    }
  },
  mutations: {
    setCartList(state, data) {
      state.cartList = data;
    }
  },
  actions: {
    queryList({ dispatch, commit, state }, {id, pageIndex}) {
      console.log(id, 'id');
      axios.get('list.json').then(res => {
        commit('setCartList', res.data.result)
      })
      dispatch('queryAAAA')
    },
    queryAAAA({ dispatch, commit, state }) {
      setTimeout(() => {
        console.log('object');
      }, 1000)
    }
  },
  modules: {
    user
  }
})

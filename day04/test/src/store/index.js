import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    list: []
  },
  getters: {
  },
  mutations: {
    setList(state, data) {
      state.list = data;
    },
    addList(state, obj) {
      state.list.push({
        id: new Date().getTime(),
        ...obj, 
        category: obj.category.join('/')
      });
    }
  },
  actions: {
  },
  modules: {
  }
})

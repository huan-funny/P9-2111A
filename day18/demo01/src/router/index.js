import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    meta: {
      title: '首页'
    },
    component: HomeView,
    children: [
      {
        path: 'users',
        name: 'users',
        meta: {
          title: '用户管理'
        },
        component: () => import('../views/users'),
        children: [
          {
            path: 'user-list',
            name: 'user-list',
            meta: {
              title: '用户列表'
            },
            component: () => import('../views/user-list')
          }
        ]
      },
      {
        path: 'rights',
        name: 'rights',
        meta: {
          title: '权限管理'
        },
        component: () => import('../views/users'),
        children: [{
          path: 'roles',
          name: 'roles',
          meta: {
            title: '角色管理'
          },
          component: () => import('../views/roles')
        }]
      }
    ]
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login')
  }
]

const router = new VueRouter({
  routes
})
// 把不需要进行token校验的页面，放进白名单
const whiteList = ['/login'];
// 登录守卫校验
router.beforeEach((to, from, next) => {
  // if (!localStorage.getItem('token') && to.path !== '/login') {

    // whiteList.indexOf('/login') // 0 // 不需要再跳转到login页面
    // whiteList.indexOf('/users/user-list') // -1 // 需要
    // whiteList.indexOf('/rights/roles') // -1 // 需要
  // if (!localStorage.getItem('token') && whiteList.indexOf(to.path) === -1) {

    // whiteList.includes('/login') // true // 不需要再跳转到login页面
    // whiteList.includes('/users/user-list')  // false
    // whiteList.includes('/rights/roles') // false
  if (!localStorage.getItem('token') && !whiteList.includes(to.path)) {
    next('/login'); // 若没有token 跳转到login页面
  }
  next();
})

export default router

const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api': {
        target: 'https://www.vue-js.com',
        changeOrigin: true // 是否开启跨域
      },
      '/admin': {
        target: 'http://leju.bufan.cloud',
        changeOrigin: true // 是否开启跨域
      },
      '/lejuAdmin': {
        target: 'http://leju.bufan.cloud',
        changeOrigin: true // 是否开启跨域
      }
    }
  }
})

/**
 * this 指向
 *  不会在函数定义时确定函数中this指向，而是在函数调用时确定this指向，谁调用了函数，那this就指向谁
 */

var name1 = '张三'
function fun1() {
  console.log(this)
}
fun1()
fun1.apply();
fun1.call(null);
fun1.call(undefined);
//  this指向window, call、apply、bind若第一个参数为空、null或undefined，这三种情况，this都指向window

// 当call、bind、apply第一个参数传递的是基本数据类型（null、undefined除外），那this就指向与之对应的对象类型
fun1.call(1111); // Number{}
fun1.call('dddd'); // String{}
fun1.call(true); // Boolean{}

/**
 * call: 第一个参数都是this要指向的对象，第二个参数是一个参数列表
 * apply: 第一个参数都是this要指向的对象，第二个参数是一个数组
 * bind: 第一个参数都是this要指向的对象, 第二个参数是一个参数列表
 * 
 * 
 * bind返回的是一个函数，需要再去调用次函数，而call、apply是立即执行的
 * bind 传递的参数可以分批传递，可以在使用bind时传递一部分，另一部分可以在调用bind所返回的函数时传递
 * bind 返回的函数可以使用new关键字创建一个实例对象
 */
let obj1 = {
  name: '张三',
  age: 18,
  sayHello: function(prop) {
    console.log(this.name, prop, 'prop')
  }
}
let obj2 = {
  name: '李四',
  age: 18,
  sayHello: function(prop) {
    console.log(this.name, prop, 'prop')
  }
}
obj1.sayHello() // 张三
obj2.sayHello() // 李四
obj1.sayHello.call(obj2, 112)
obj1.sayHello.apply(obj2, [112])
obj1.sayHello.bind(obj2)

Function.prototype.myCall = function(obj, ...rest) {
  if (!obj.length || obj === null || obj === undefined) {
    obj = globalThis; // 就是全局的this指向，也就是window
  }else if (typeof obj !== 'object') {
    obj = new Object(obj) // 将传递进来的基本数据转成对象类型
  }
  let key = new Symbol() // 创建出来一个唯一的key
  // obj.1 = this
  // obj.a = this
  obj[key] = this; // 根据谁调用函数this指向指向的特点，来确定调用的函数
  let res = obj[key](...rest);
  delete obj[key]; // 删除对象中的某个属性
  return res;
}
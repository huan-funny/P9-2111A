console.log('12345678wert')

/* 声明变量
let 变量名: 变量类型 = 所属变量类型内容
*/
let str:String = 'sdfghj';
str = '1';

let num: Number;
num = 1;

// 非严格模式下，undefined 与null 是所有类型的子类型
let num1:Number = undefined;

// let num2:undefined = undefined;

// void 空类型, 常用于函数没有返回值的情况下
function fun1():void {
  console.log('asdfghjsdfghj')
}

let arr1:Array<Number> = [1,2,3]; // 指定一个数组中每一项是number类型
let arr3:number[] = [1,2,3]; // 与上面对等

let arr2:Array<String> = [];
let arr4:string[] = ['1', '2']

let arr5:Array<Number | String | Boolean> = [1,'2', true, false]; // 指定数组中可以是number类型 也可以是 string

let arr6:Array<any> = [1,2,3,'4', true, {}, []]

let dd: any = '1234t'; // 任意类型, 不确定值为什么类型时，可定义为any类型
dd = 111;
dd = {};
dd = true

type Tsex = '男' | '女';
type Tstatus = 'primary' | 'success' | 'warning' | 'danger' | 'info' | 'text';

interface Iobj {
  readonly id?: Number; // 此属性为只读
  name: String;
  age: Number;
  type?: Tstatus;
  sex?: Tsex; // 加？表示此变量可有可无
  [prop: string]: any // 表示不确定的属性
}
// a.b?.c

let obj:Iobj = {
  name: '章四',
  age: 18,
  work: '前端开发',
  job: 123,
  type: 'success'
}
let obj1: Iobj = {
  id: 112,
  name: '张三',
  age: 20,
  sex: '男'
}
// obj1.id = 123; // 不可修改

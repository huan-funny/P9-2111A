let arr = [1,2,3,4,5,6];

// push: 尾部添加；返回值是修改后数组的长度；会改变原数组
let prop = arr.push(7,4);
// console.log(prop, 'prop'); 8 
// console.log(arr, 'arr');

// unshift：头部添加；返回值是修改后数组的长度；会改变原数组
let prop1 = arr.unshift(9,34, {
  a: 1
}, [12,4]);
// console.log(prop1, 'prop1');
// console.log(arr, 'arr');

/**
 * splice:
 *  添加：（要添加位置的下标, 0, 要添加的数据列表）
 *  删除:（要删除的下标, 删除个数）
 *  替换: (要替换的开始下标, 替换的个数, 替换后的数据)
 *  */ 
arr.splice(3, 0, 1, {b: 2});
console.log(arr, 'arr');
arr.splice(3, 2);
console.log(arr, 'arr');
arr.splice(1, 1, 10)
console.log(arr, 'arr');


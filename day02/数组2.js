let arr = [1,2,3,4,5,6];

// some: 返回布尔值，只要有满足条件的元素，就会返回true，否则返回false
let someItem = arr.some(item => item >= 7);
console.log(someItem, 'someItem');

// every: 所有的元素都要满足条件，才会返回true，只要一个不满足就返回false
let everyItem = arr.every(item => item >= 1);
console.log(everyItem, 'everyItem');

let cart = [{ id: 1, flag: true }, { id: 2, flag: true }, { id: 3, flag: true }];
let selectAll = cart.every(item => item.flag);
console.log(selectAll, 'selectAll')

// includes: 返回布尔值，查找是否包含某个元素, 内部是通过全等判断的
// let newArr = arr.filter(item => item === 5);
// console.log(newArr.length > 0);
let exit5 = arr.includes(5);
console.log(exit5, 'exit5');

// indexOf: 查找某个元素的下标， 没有找到返回 -1
let index = arr.indexOf(1);
// console.log(index >= 0, 'index', index !== -1); // 判断是否有某个属性的方法

// split： 字符串转数组；以什么字符串分隔，那括号里就写什么字符串
let categoryStr = '异国料理/日料/刺身';
let categoryArr = categoryStr.split('/');
console.log(categoryArr, 'categoryArr');

// join: 数组转字符串；想以什么字符串分隔，那括号里就写什么字符串
let newCategory = [ '异国料理', '日料', '刺身' ];
let newCategoryStr = newCategory.join('/');
console.log(newCategoryStr, 'newCategoryStr');

// sort: 排序；默认从小到大排序，
let dArr = [6,'3',4,'5',2,6,1];
let newDArr = dArr.sort((a,b) => {
  return b - a
});
console.log(newDArr, 'newDArr');
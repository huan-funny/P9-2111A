console.log('12345678wert');
/* 声明变量
let 变量名: 变量类型 = 所属变量类型内容
*/
let str = 'sdfghj';
str = '1';
let num;
num = 1;
// 非严格模式下，undefined 与null 是所有类型的子类型
let num1 = undefined;
// let num2:undefined = undefined;
// void 空类型, 常用于函数没有返回值的情况下
function fun1() {
    console.log('asdfghjsdfghj');
}
let arr1 = [1, 2, 3]; // 指定一个数组中每一项是number类型
let arr3 = [1, 2, 3]; // 与上面对等
let arr2 = [];
let arr4 = ['1', '2'];
let arr5 = [1, '2', true, false]; // 指定数组中可以是number类型 也可以是 string
let arr6 = [1, 2, 3, '4', true, {}, []];
let dd = '1234t'; // 任意类型, 不确定值为什么类型时，可定义为any类型
dd = 111;
dd = {};
dd = true;
// a.b?.c
let obj = {
    name: '章四',
    age: 18,
    work: '前端开发',
    job: 123,
    type: 'success'
};
let obj1 = {
    id: 112,
    name: '张三',
    age: 20,
    sex: '男'
};
// obj1.id = 123; // 不可修改

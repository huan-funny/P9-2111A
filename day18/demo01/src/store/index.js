import Vue from 'vue'
import Vuex from 'vuex'
import persit from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [persit()],
  state: {
    allList: []
  },
  getters: {
  },
  mutations: {
    setAllList(state, list) {
      state.allList = list;
    },
    delAllList(state, index) {
      state.allList.splice(index, 1); // 对所有的数据进行删除
    },
    editAllList(state, data) {
      let index = state.allList.findIndex(item => item.id == data.id);
      state.allList[index] = {
        ...state.allList[index],
        ...data
      };
    },
    addAllList(state, data) {
      let time = new Date().getTime();
      state.allList.push({
        ...data,
        id: time,
        mg_state: false,
        create_time: time,
      });
    },
    changeState(state, data) {
      let index = state.allList.findIndex(item => item.id == data.id);
      state.allList[index].mg_state = data.mg_state;
    }
  },
  actions: {
  },
  modules: {
  }
})

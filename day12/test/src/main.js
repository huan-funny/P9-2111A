import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant'
import 'vant/lib/index.css';
import 'amfe-flexible';
import './style/iconfont.css'

Vue.use(Vant);

Vue.config.productionTip = false

Vue.directive('bgColor', (el, binding, vnode) => {
  el.style.backgroundColor = store.state.bgColor
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

let obj = {
  name: '张山',
  age: 18,
  job: 1500,
  work: '',
  work1: undefined
};
obj.job
obj.age
let jsonObj = {
  prop: 'a'
}
// 动态给对象添加或访问属性（属性名表达式）
obj[jsonObj.prop] = 'aaaa';
console.log(obj, 'obj');

// 解构赋值
// 只有当属性值为undefined时，默认值才会生效, 若对象中没有该属性，默认值也会生效
const { name, age, job, work = '前端开发', work1 = '前端开发', work2 = '前端开发' } = obj;
// const name = obj.name
// console.log(work, 'work', work1, 'work1', work2, 'work2')

let obj1 = {
  a: '123',
  b: 234,
  c: '尚平',
  d: {
    e: '111111'
  }
};
// 合并对象时，若后面对象与前面对象中的属性重复时，后面的属性会覆盖前面的
let newObj = {...obj1, ...obj};
newObj.c = '的的'; // 此时obj1不会被改变，因为c是简单数据类型
// console.log(obj1, 'obj1')
newObj.d.e = '222222222'; // 此时obj1会发生改变，因为d是复杂数据类型
// console.log(obj1, 'obj1')

// Object.assign(目标对象, 对象列表) 将对象列表中的属性复制到目标对象上
let newobj1 =Object.assign({}, obj1, obj);
// console.log(newobj1, 'newobj1');
// console.log(obj1, 'obj1');
newobj1.c = '当然';
console.log(obj1, 'obj1');
newobj1.d.e = '3333333';
console.log(obj1, 'obj1');

/* 总结：
  扩展运算符、Object.assign() 这两个拷贝的是浅拷贝
*/

// JSON.parse(JSON.stringify(对象)) 深拷贝
// 缺点：不能将值为undefined、函数拷贝过去
let copyOjb = {
  name: '张三',
  age: null,
  sex: undefined,
  fun: function() {}
}
let newCopyOjb = JSON.parse(JSON.stringify(copyOjb));
console.log(newCopyOjb, 'newCopyOjb');


function pro1() {
  return new Promise((resolve, reject) => {
    reject('错误信息');
  })
}
async function fun1() {
  return '11111' // 返回正确结果，将promise对象的状态改为fulfilled
}

async function fun1() {
  try {
    let res = await pro1
    console.log(res)
  } catch (error) {
    throw error // 抛出错误结果，将promise对象的状态改为rejected
  }
}
fun1().catch(error => {
  console.log(error, 'error'); // 2222222
})

// 使用async声明的函数，可以使用链式调用，那链式调用的方法中回调函数的参数的值就是 async函数中返回的值
async function fun1() {
  return '11111'
}
fun1().then((res) => {
    console.log(res, 'res')
})
// await 等待，会造成之后的代码形成阻塞，只有当await的结果返回之后，才会执行
async function aa() {
  // let res = await fun1(); // 如果await 后面跟的是一个promise对象，需要等待promise的执行结果，执行结果的值是await所等待的值
  let res = await Promise.resolve('2222');
  // let res = await 22222; // 而await如果后面跟的是一个常量，那这个常量就是await等待的值
  console.log(res)
}

// p1 p2 
// promise.all 
// promise.race()

async function Async1() {
  setTimeout(() => {
    return '11111'
  }, 3000)
}

async function Async2() {
  setTimeout(() => {
    return '11111'
  }, 2000)
}

Promise.all([Async1(), Async2()]).then(res => {
  console.log(res)
});

async function fun2() {
  // let res1 = await Async1();
  // let res2 = await Async2();
  // console.log(res1, 'res', res2, 'res2')
  let res1 = Async1();
  let res2 = Async2();
  let res11= await res1;
  let res22 = await res2;
  console.log(res11, 'res11',res22, 'res22')
}

/**
 * 常见的宏任务：
 *  setTimeout() \ setInterval() \ script
 * 
 * 常见的微任务：
 *  promise.then / .catch、
 */
// 同步任务、异步任务（）
// 同步任务在主线程中执行，它会先进先出的顺序去执行
// 异步任务中会先执行宏任务，当当前宏任务执行完之后，会在当前宏任务中查找是否有微任务，当所有的微任务执行完之后，会去查找下一个宏任务


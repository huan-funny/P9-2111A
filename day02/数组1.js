let arr = [1,2,3,4,5,6];

// slice:截取,（开始位置,结束位置）包左不包右
let newArr = arr.slice(2, 5);
// console.log(newArr, 'newArr');

// 截取到最后
// let newArr1 = arr.slice(1, arr.length);
let newArr1 = arr.slice(1);
// console.log(newArr1, 'newArr1');

// 截取到倒数第一位
arr.slice(1, -1);

// concat：合并数组，当修改简单类型的数组时是深拷贝，当修改复杂类型的数组时是浅拷贝
let arr1 = [0, 9, 8];
let concatArr = arr.concat(arr1);
concatArr[2] = 5;
// console.log(concatArr, 'concatArr');
// console.log(arr, 'arr');

let arr2 = [{id: 1, num: 1}, {id: 2, num: 2}];
let arr3 = [{id: 3, num: 3}, {id: 4, num: 4}];
let concatArr1 = arr2.concat(arr3);
concatArr1[0].num = 2;
console.log(concatArr1, 'concatArr1');
console.log(arr2, 'arr2');

// 合并数组：扩展运算符
let concatArr2 = [...arr2, ...arr3];

// filter: 过滤符合条件的数组, 返回新的数组; 不会改变原数组
// let filterArr = arr.filter(item => item >= 3);
let filterArr = arr.filter(item => {
  return item >= 3;
})
// console.log(filterArr, 'filterArr');

// find: 查找符合条件的元素; 返回的是符合条件的第一个元素；若没找符合条件的元素，则返回undefined
let findProp = arr.find(item => item >= 3);
console.log(findProp, 'findProp');

let findProp1 = arr.find(item => item >= 7);
console.log(findProp1, 'findProp1');

// findIndex: 找符合条件的元素下标；返回的是符合条件的第一个元素下标；若没找符合条件的元素，则返回 -1
let index = arr.findIndex(item => item >= 5);
console.log(index, 'index');

let index1 = arr.findIndex(item => item >= 7);
console.log(index1, 'index1');

// findLastIndex: 从后往前找符合条件的第一个元素的下标；若没找符合条件的元素，则返回 -1
// let lastIndex = arr.findLastIndex(item => item >= 5);
// console.log(lastIndex, 'lastIndex'); // 5

// let lastIndex1 = arr.findLastIndex(item => item >= 7);
// console.log(lastIndex1, 'lastIndex1'); // -1


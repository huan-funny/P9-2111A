import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tableList: []
  },
  getters: {
  },
  mutations: {
    setTableList(state, data) {
      state.tableList = data;
    },
    // 批量删除
    dels(state, ids) {
      state.tableList = state.tableList.filter(item => !ids.includes(item.id));
    },
    edit(state, obj) {
      let index = state.tableList.findIndex(item => item.id === obj.id);
      // state.tableList[index] = obj;
      Vue.set(state.tableList, index, obj)
    },
    // 单个删除
    del(state, id) {
      let index = state.tableList.findIndex(item => item.id === id);
      state.tableList.splice(index, 1);
    }
  },
  actions: {
    qeryList({ dispatch, commit, state }) {
      axios.get('table.json').then(res => {
        commit('setTableList', res.data.list)
      });
    }
  },
  modules: {
  }
})

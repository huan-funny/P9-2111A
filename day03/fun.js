// 函数的声明
/**
 * 命名函数 、函数表达式（匿名函数）
 */
fun1();
// 命名函数：调用可在函数声明的前面调用，也可以在后面调用
function fun1() {
  console.log('111111')
}
// fun1();

// 匿名函数：调用只可在函数声明的后面调用
var fun2 = function () {
  console.log('222222')
}
fun2();

// 函数的返回值
/**
 * 
 * return 返回只能返回一个值
 * 若函数没有返回值，你的函数的值就是undefined
 */
function fun3() {
  return '33333';
}
console.log(fun3())

function fun4() {
  return ['333333','111111'];
}

// 函数参数
/**
 * 函数参数的默认值，必须设置到最后的参数
 */
function fun5(num1, num2) {
  let newNum2 = num2 || 5;
  return num1 + newNum2
}
fun5(1);

function fun6(num1, num2 = 5) {
  return num1 + num2;
}
fun6(1)

function fun7() {
  console.log(arguments, 'arguments')
  // let sum = 0;
  // for(let i = 0; i< arguments.length; i++) {
  //   sum += arguments[i];
  // }
  // return sum;
  let arr = Array.from(arguments); // 转成真正的数组
  return arr.reduce((prev, current) => { // 计算和
    return prev + current
  }, 0)
}

fun7(1,2);
fun7(3,4,5,6,7);

// rest 剩余参数，只能放在具体参数的后面，它是一个数组
function fun8(num1, ...rest) {
  console.log(rest, 'rest')
  return rest.reduce((prev, current) => { // 计算和
    return prev + current
  }, num1);
}
fun8(1,2);
fun8(3,4,5,6,7);

function fun9(...rest) {
  console.log(rest, 'rest')
  return rest.reduce((prev, current) => { // 计算乘
    return prev * current
  }, num1);
}
fun9(1,2);
fun9(3,4,5,6,7);

// 箭头函数的this，当函数定义时，this指向已确定，指向父亲所在作用域
var fun10 = () => {
  console.log(this, 'this')
}
// 箭头函数自带return
var fun11 = (num1, num2 = 10) => {
  console.log(11111)
  return num1 + num2
}